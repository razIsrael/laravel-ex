<?php
class Htmalpage {
    protected $titel;
    protected $body;
    
    public function view(){

        echo "<html>
        <head>
          <title>$this->titel</title>
        </head>
        <body>
        $this->body
        </body>
        </html>
        ";
    }

    function __construct($titel,$body){
        $this->titel = $titel;
        $this->body = $body;
    }
}

class coloredHtmalpage extends Htmalpage{
    
    protected $color = 'red';
    
    public function __set($property,$value){
        
        if($property == 'color'){
            $colors = array('red','yellow','green');
            if(in_array($value,$colors)){
                $this->color = $value;
            }else {
                echo "<p > Please select defrint color </p>";
            }
        }

    }

    public function show(){

        echo "<html>
        <head>
          <title>$this->titel</title>
        </head>
        <body style = 'color:$this->color'>
        $this->body
        </body>
        </html>
        ";
        //echo "<p style = 'color:$this->color'> $this->text </p>";
    }
}

function showObject($object){
    $object->show();
}

class sizeHtmalpage extends coloredHtmalpage{
    
    protected $size = 11;
    
    public function __set($property,$value){
        
        if($property == 'size'){
            if($value<=24 && $value>=10){
                $this->size = $value;
            }else {
                $message = "wrong input, number must be betwin 10-24";
                die( "<script type='text/javascript'>alert('$message');</script>");
            }
        }elseif($property == 'color'){
            $colors = array('red','yellow','green');
            if(in_array($value,$colors)){
                $this->color = $value;
            }else {
                die ("<p > Please select defrint color </p>");
            }
        }

    }

    public function show(){
$size = "$this->size.px";
        echo "<html>
        <head>
          <title>$this->titel</title>
        </head>
        <body style = 'color:$this->color;font-size:$this->size;'>
        $this->body
        </body>
        </html>
        ";
      
    }
}

?>